<?php
declare(strict_types=1);

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class ParseNamespacesCommandTest
 *
 * @package App\Tests\Command
 */
class ParseNamespacesCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $this->markTestSkipped();
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('app:parse-symfony-namespaces');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('', $output);
    }
}
