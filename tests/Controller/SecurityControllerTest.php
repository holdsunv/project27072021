<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SecurityControllerTest
 *
 * @package App\Tests\Controller
 */
class SecurityControllerTest extends WebTestCase
{
    public function testRegisterAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/register');
        $button = $crawler->selectButton('user_create[submit]');
        $form = $button->form([
            'user_create[username]' => 'john_admin',
            'user_create[password][first]' => 'john789',
            'user_create[password][second]' => 'john789',
        ]);
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }

    public function testLoginAction()
    {
        $client = static ::createClient();
        $crawler = $client->request('GET', '/article/login');
        $button = $crawler->selectButton('login-button');
        $form = $button->form([
            'username' => 'john_admin',
            'password' => 'john789',
        ]);
//        var_dump($var);exit;
        $client->submit($form);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}
