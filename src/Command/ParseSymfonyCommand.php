<?php

namespace App\Command;

use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ParseSymfonyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ParseSymfonyCommand constructor.
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:parsesymfony')
            ->setDescription('Parsing site api.symfony.com')
            ->setHelp('This command parses the site ...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $html = file_get_contents('http://api.andreybolonin.com');

        $crawler = new Crawler($html);
        $filtered = $crawler->filter('div.namespace-container > ul > li > a');

        foreach ($filtered as $value) {
            $url = 'http://api.andreybolonin.com/'.str_replace('../', '', $value->getAttribute('href'));
            $set = new NamespaceSymfony();
            $set->setName($value->textContent);
            $set->setUrl($url);
            $this->em->persist($set);
        }
        $this->em->flush();
//        var_dump($elements->count());
        return 1;
    }
}
